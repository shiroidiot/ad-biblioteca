var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'ad-biblioteca'
    },
    port: 3000,
    db: 'mysql://root:root@33.33.33.1/ad_biblioteca-development'
  },

  test: {
    root: rootPath,
    app: {
      name: 'ad-biblioteca'
    },
    port: 3000,
    db: 'mysql://root:root@33.33.33.1/ad_biblioteca-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'ad-biblioteca'
    },
    port: 3000,
    db: 'mysql://root:root@33.33.33.1/ad_biblioteca-production'
  }
};

module.exports = config[env];
