module.exports = function (sequelize, DataTypes) {
  var Loan = sequelize.define('Loan', {
    date_start: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    date_end: {
      type: DataTypes.DATE,
      allowNull: false
    },
    returned: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    }
  }, {
      classMethods: {
        associate: function (models) {
          Loan.belongsTo(models.Book);
          Loan.belongsTo(models.Member);
        }
      }
    });

  return Loan;
};