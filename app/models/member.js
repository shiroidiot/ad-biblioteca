module.exports = function (sequelize, DataTypes) {
  var Member = sequelize.define('Member', {
    name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    age: DataTypes.INTEGER,
    address: DataTypes.STRING,
    phone: DataTypes.STRING
  }, {
      classMethods: {
        associate: function (models) {
          Member.hasMany(models.Loan);
          Member.hasMany(models.Book, {
            through: models.Loan
          });
        }
      }
    });

  return Member;
};