module.exports = function (sequelize, DataTypes) {
  var Book = sequelize.define('Book', {
    title: DataTypes.STRING,
    author: DataTypes.STRING,
    pages: DataTypes.INTEGER,
    total: DataTypes.INTEGER,
    publisher: DataTypes.STRING,
    pub_year: DataTypes.DATE
  }, {
      classMethods: {
        associate: function (models) {
          Book.hasMany(models.Loan);
          Book.hasMany(models.Member, {
            through: models.Loan
          });
        }
      }
    });

  return Book;
};