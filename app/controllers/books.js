var express = require('express'),
  moment = require('moment'),
  router = express.Router(),
  db = require('../models');

module.exports = function (app) {
  app.use('/books', router);
};

router.get('/', function (req, res, next) {
  db.Book.findAll().then(function (books) {
    res.render('books', {
      title: 'Libros',
      books: books
    });
  });
});

router.post('/', function (req, res, next) {
  db.Book.upsert({
    id: req.body.id,
    title: req.body.title,
    author: req.body.author,
    total: req.body.total,
    publisher: req.body.publisher,
    pub_year: (new Date ((new Date((new Date(new Date(req.body.year))).toISOString() )).getTime() - ((new Date(req.body.year)).getTimezoneOffset()*60000))).toISOString().slice(0, 19).replace('T', ' '),
    pages: req.body.pages
  }).then(function (book) {
    res.redirect('/books');
  }).error(function (err) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(err));
  });
});

router.get('/delete/:id([0-9]+)', function (req, res, next) {
  db.Book.destroy({
    where: {
      id: req.params.id
    }
  }).then(function () {
    res.redirect('/books');
  });
});

router.get('/edit/:id([0-9]+)', function (req, res, next) {
  db.Book.findOne({
    where: {
     id: req.params.id 
    }
  }).then(function (book) {
    res.render('books_edit', {
      title: 'Libros/Editar',
      book: book
    });
  });
});

router.get('/new', function (req, res, next) {
  res.render('books_edit', {
    title: 'Libros/Nuevo',
    book: {id: '', title: '', author: '', total: '', publisher: '', pub_year: '', pages: ''},
  });
});