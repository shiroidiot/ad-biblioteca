var express = require('express'),
  router = express.Router(),
  db = require('../models');

module.exports = function (app) {
  app.use('/members', router);
};

router.get('/', function (req, res, next) {
  db.Member.findAll().then(function (members) {
    res.render('members', {
      title: 'Socios',
      members: members
    });
  });
});

router.post('/', function (req, res, next) {
  db.Member.upsert({
    id: req.body.id,
    name: req.body.name,
    last_name: req.body.last_name,
    age: req.body.age,
    address: req.body.address,
    phone: req.body.phone
  }).then(function (member) {
    res.redirect('/members');
  }).error(function (err) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(err));
  });
});

router.get('/delete/:id([0-9]+)', function (req, res, next) {
  db.Member.destroy({
    where: {
      id: req.params.id
    }
  }).then(function () {
    res.redirect('/members');
  });
});

router.get('/edit/:id([0-9]+)', function (req, res, next) {
  db.Member.findOne({
    where: {
     id: req.params.id 
    }
  }).then(function (member) {
    res.render('members_edit', {
      title: 'Usuarios/Editar',
      member: member
    });
  });
});

router.get('/new', function (req, res, next) {
  res.render('members_edit', {
    title: 'Usuarios/Nuevo',
    member: {id: '', name: '', last_name: '', age: '', address: '', phone: ''},
  });
});