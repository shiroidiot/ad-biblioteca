var express = require('express'),
  router = express.Router(),
  db = require('../models');

module.exports = function (app) {
  app.use('/loans', router);
};

router.get('/', function (req, res, next) {
  db.Loan.findAll({
    include: [{all: true, nested: false}],
  }).then(function (loans) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(loans));
  });
});

router.post('/', function (req, res, next) {
  db.Loan.create({
    BookId: req.body.book,
    MemberId: req.body.member,
    date_end: new Date(new Date(new Date().toDateString()).getTime() + 60 * 60 * 24 * 8 * 1000 - 1)
  }).then(function (loan) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(loan));
  }).error(function (err) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(err));
  });
});